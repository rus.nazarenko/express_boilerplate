import express, { json, Application } from 'express'
import cookieParser from 'cookie-parser'
import { port } from './config/server'
import userRouter from "./components/user/userRouter"
import someRouter from "./components/some/someRouter"
import { tokenVerification } from './middleware/tokenVerification'
import { logging } from './middleware/logging'

const app: Application = express()


app.use(logging())
// app.use(bodyParser.json())
app.use(json())
app.use(cookieParser())
app.use(express.static('static'))


// Routes without authentication
app.use('/api/user', userRouter)

// Authenticated routes
// app.use('/api/auth', tokenVerification)
app.use('/api/some', tokenVerification, someRouter)

app.listen(port, () => {
  console.log(`The server is running on http://localhost:${port}`)
})


// db (модуль для работы с БД) (readme какие скрипты нужно запускать для миграций)
// config
// server (с эндпоинтами)
// --user
// middleware
// --auth-middleware
// --log-middleware (лог файлы должны создаваться каждый день)
// настройки докер
// скрипт для старта

// TS, 
// ()

// Библиотеки для логов:
// pino
// winston

// КРОН задача.
// Миграции оставлять на js.
// Модели на ts.

// 2docker файла (*):
// 1. для дева - с nodemon
// 2. мультистейж (без ts)


// Домашнее задание - сделать стратер(бойлерплейт) с использованием express, sequelize. Должен включать в себя ридми файлы со всеми командами необходимыми для старта приложения, в стартере реализовать логирование в файл, сборку докера ( multistage build), несколько ендпоинтов для создания юзера, и авторизации. Миграциями создавать необходимые таблицы в базе данных