import { registerUserService, loginUserService } from './userService'
import { User } from './userTypes'
import { Request, Response } from 'express'



export const registerUserController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const data: User = {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    }
    const newUser = await registerUserService(data)

    res.status(201).json({ success: true, newUser })
  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}



export const loginUserController = async (req: Request, res: Response) => {
  try {
    if (Object.entries(req.body).length === 0) { throw new Error("No data") }

    const data: User = {
      password: req.body.password,
      email: req.body.email
    }
    const token = await loginUserService(data)
    res.cookie('token', `Bearer ${token}`)
    res.status(201).json({ success: true, token: token })

  } catch (err) {
    res.status(401).json({ success: false, error: err.message })
  }
}