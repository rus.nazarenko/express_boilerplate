FROM node:14.17.3		

WORKDIR /server

COPY package*.json ./

RUN npm install

COPY . . 		

ENV TZ Europe/Kiev

CMD npm run start